# dotfiles
  Collection of some of my commonly used dotfiles/configs

In MacOS, set Caps Lock to function as Control, Key Repeat to fast
Browser: Firefox Developer Edition
Terminal: iTerm2
Color Scheme: Gruvbox Light (https://github.com/gruvbox-community/gruvbox/)
Font: Go Mono (https://blog.golang.org/go-fonts)
